package mama.ndklib;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;

import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2015/7/6.
 */
public class JC {
    static {
        System.loadLibrary("ndklib");
    }

    private static Context appContext;

    public static Context getAppContext() {
        return appContext;
    }

    public static void setAppContext(Context context) {
        if (context != null) {
            appContext = context.getApplicationContext();
        }
    }

    public static void init(Context context) {
        setAppContext(context);
      /*  initNative();
        Log.i("JCC", "Java str: " + nativeGenTokenUrl("url", "key"));
        int[] arr = new int[10];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = i;
        }
        int sum = sumArray(arr);
        Log.i("JCC", "sum = " + sum);
        callJavaStaticMethod();
        callJavaInstaceMethod();*/


        ClassField obj = new ClassField();
        obj.setNum(10);
        obj.setStr("Hello");

        // 本地代码访问和修改ClassField为中的静态属性num
        //accessStaticField();
        accessInstanceField(obj);

        // 输出本地代码修改过后的值
        //  System.out.println("In Java--->ClassField.num = " + obj.getNum());
        Log.i("JCC", "ClassField.str = " + obj.getStr());
        //getPublicKey();



        Map<String,Object> map=new HashMap<String,Object>();
        map.put("id",1);
        map.put("name","alvin");
        buildParam(map);
    }

    public static native String nativeGenTokenUrl(String url, String keyName);

    /**
     * native初始化
     */
    private static native void initNative();

    // 在本地代码中求数组中所有元素的和
    private static native int sumArray(int[] arr);

    public static native void callJavaStaticMethod();

    public static native void callJavaInstaceMethod();


    /**
     * C/C++访问类的实例变量和静态变量
     *
     * @param obj
     */
    private native static void accessInstanceField(ClassField obj);

    //private native static void accessStaticField();
    private native static void buildParam(Map<String,Object> map);

    /**
     * 获取是不是Debugable
     *
     * @return
     */
    public static boolean isDebugable() {
        if (appContext == null) {
            return false;
        }

        return (appContext.getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
    }


    /**
     * 获取签名
     */
    private static  byte[] getSing() {
        if (appContext == null) {
            return null;
        }
        byte[] result = null;
        try {
            PackageInfo pi = appContext.getPackageManager().getPackageInfo(appContext.getPackageName(), PackageManager.GET_SIGNATURES);
            Signature signature = pi.signatures[0];
            if (signature != null) {
                result = signature.toByteArray();
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return result;

    }

    /**
     * 获取公钥
     */
    private static byte[] getPublicKey() {
        byte[] signature = getSing();
        if (null == signature) {
            return null;
        }
        try {
            CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
            X509Certificate cert = (X509Certificate) certFactory.generateCertificate(new ByteArrayInputStream(signature));
            return cert.getPublicKey().getEncoded();

        } catch (CertificateException e) {
            e.printStackTrace();
        }

        return null;


    }
}
