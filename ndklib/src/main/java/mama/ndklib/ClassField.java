package mama.ndklib;

/**
 * C++访问JAVA对象变量
 * Created by Administrator on 2015/7/15.
 */
public class ClassField {
    private static int num;

    private String str;

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        ClassField.num = num;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
}
