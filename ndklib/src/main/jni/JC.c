#include <android/log.h>
#include <jni.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "mama_ndklib_JC.h"
#include "define.h"
#define LOG_TAG "JCC"
#define LOGD(...) __android_log_print(ANDROID_LOG_DEBUG,LOG_TAG ,__VA_ARGS__) // 定义LOGD类型
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO,LOG_TAG ,__VA_ARGS__) // 定义LOGI类型
#define LOGW(...) __android_log_print(ANDROID_LOG_WARN,LOG_TAG ,__VA_ARGS__) // 定义LOGW类型
#define LOGE(...) __android_log_print(ANDROID_LOG_ERROR,LOG_TAG ,__VA_ARGS__) // 定义LOGE类型
#define LOGF(...) __android_log_print(ANDROID_LOG_FATAL,LOG_TAG ,__VA_ARGS__) // 定义LOGF类型
JNIEXPORT void JNICALL Java_mama_ndklib_JC_initNative
  (JNIEnv *env, jclass clazz){
       int i = 0;
       LOGD("########## i = %d", i);
  }

  JNIEXPORT jstring JNICALL Java_mama_ndklib_JC_nativeGenTokenUrl
    (JNIEnv *env, jclass clazz, jstring params, jstring key){

         /*  const char *c_str = NULL;
          char buff[128]="hello ";
          jboolean isCopy;
          c_str=(*env)->GetStringUTFChars(env,key,NULL);
          if(c_str == NULL){
                 return NULL;
           }
          LOGI("result:%s \n",c_str);
          sprintf(buff, "hello %s", c_str);
          (*env)->ReleaseStringUTFChars(env,key,c_str);
          return (*env)->NewStringUTF(env,buff);*/

        /*    *//**
            计算长度
            *//*
            jsize len = (*env)->GetStringLength(env,key);	// 获取unicode字符串的长度（java是以用unicode编码的）
          	LOGI("\nstr len:%d\n",len);
          	char buff[128] = "hello ";
          	char* pBuff = buff + 6;
          	// 将java中的字符串以utf-8编码拷入C缓冲区,该函数内部不会分配内存空间
          	(*env)->GetStringUTFRegion(env,key,0,len,pBuff);
          	//(*env)->GetStringRegion(env,key,0,len,pBuff);	// 以utf16编码拷与c缓冲区
*/
          	/**字符复制
          	*/

          	char *strA="Test strcpy()";
          	char strDest[100];
          	char *strcpy2=strcpy(strDest, strA);
          	 LOGI("copy resylt:%s\n",strcpy2);

             strcpy2=strncpy(strDest,"c.code",2);
             LOGI("copy resylt:%s\n",strcpy2);

          	return (*env)->NewStringUTF(env,strcpy2);
    }

    JNIEXPORT jint JNICALL Java_mama_ndklib_JC_sumArray(JNIEnv *env, jobject obj, jintArray j_array){
      jint i,sum=0;
      jint *c_array;
      jint arr_len;
      arr_len=(*env)->GetArrayLength(env,j_array);
      LOGI("array length is %d\n",arr_len);
      c_array=(jint*)malloc(sizeof(jint)*arr_len);//申请缓冲区
      memset(c_array,0,sizeof(jint)*arr_len);//初始化
      (*env)->GetIntArrayRegion(env,j_array,0,arr_len,c_array); // 拷贝Java数组中的所有元素到缓冲区中
      for(i=0;i<arr_len;i++){
        sum+=c_array[i];
      }
      free(c_array);// 释放存储数组元素的缓冲区
      return  sum;
    }

    JNIEXPORT void JNICALL  Java_mama_ndklib_JC_callJavaStaticMethod(JNIEnv *env, jclass clazz){
       jclass cl=NULL;
       jstring str_arg=NULL;
       jmethodID mid_static_method;
       // 1、从classpath路径下搜索ClassMethod这个类，并返回该类的Class对象
       cl=(*env)->FindClass(env,"mama/ndklib/ClassMethod");
       if(cl==NULL){
         LOGI("not find%s\n","FindClass");
         return;
       }

       // 2、从clazz类中查找callStaticMethod方法 http://blog.csdn.net/jintiaozhuang/article/details/9016889
       mid_static_method=(*env)->GetStaticMethodID(env,cl,"callStatixMethod","(Ljava/lang/String;I)V");
       if(NULL==mid_static_method){
           LOGI("not find%s\n","GetStaticMethodID");
           return;
        }


        // 3、调用clazz类的callStaticMethod静态方法
        str_arg=(*env)->NewStringUTF(env,"我是静态方法");
        (*env)->CallStaticVoidMethod(env,cl,mid_static_method, str_arg, 100);

         // 删除局部引用
        (*env)->DeleteLocalRef(env,cl);
        (*env)->DeleteLocalRef(env,str_arg);
    }

     /**
      * 实例化的访问
      */
     JNIEXPORT void JNICALL    Java_mama_ndklib_JC_callJavaInstaceMethod(JNIEnv *env, jclass clazz){
         jclass cl=NULL;
         jobject jb=NULL;
         jmethodID  mid_con=NULL;
         jmethodID mid_instance = NULL;
         jstring str_arg = NULL;

       //// 1、从classpath路径下搜索ClassMethod这个类，并返回该类的Class对象
        cl = (*env)->FindClass(env, "mama/ndklib/ClassMethod");
        if (clazz == NULL) {
           LOGI("not find%s\n","callInstanceMethod");
            return;
         }
       // 2、获取类的默认构造方法ID
       mid_con=(*env)->GetMethodID(env,cl,"<init>","()V");//<init>代表类的构造方法名称，()V代表无参无返回值的构造方法（即默认构造方法）
       if(NULL==mid_con){
        LOGI("not find%s\n","找不到默认的构造方");
         return;
        }
      // 3、查找实例方法的ID
        mid_instance = (*env)->GetMethodID(env, cl, "callInstanceMethod", "(Ljava/lang/String;I)V");

        if (mid_instance == NULL) {
           LOGI("not find%s\n","查找不到实例方法的ID");
          return;
        }

       // 4、创建该类的实例
       jb = (*env)->NewObject(env,cl,mid_con);
      if (jb == NULL) {
           LOGI("not find%s\n","在mama.ndklib.ClassMethod类中找不到callInstanceMethod方法");
           return;
      }

      //5调用对象的实例化方法
      str_arg=(*env)->NewStringUTF(env,"实例化调用");
      (*env)->CallVoidMethod(env,jb,mid_instance,str_arg,200);
     // 删除局部引用
      (*env)->DeleteLocalRef(env,cl);
      (*env)->DeleteLocalRef(env,jb);
      (*env)->DeleteLocalRef(env,str_arg);
}






JNIEXPORT void JNICALL Java_mama_ndklib_JC_buildParam(JNIEnv *env,jclass cls,jobject params){
   jclass clasParms=(*env)->GetObjectClass(env,params);
   jmethodID midkeySet=(*env)->GetMethodID(env,clasParms,"keySet","()Ljava/util/Set;");
   if(midkeySet==NULL){
      return;
  }
   jobject objSet =(*env)->CallObjectMethod(env,params,midkeySet);
   jclass clsSet=(*env)->GetObjectClass(env,objSet);
   jmethodID midToArray = (*env)->GetMethodID(env, clsSet, "toArray", "()[Ljava/lang/Object;");
   if(midToArray==NULL){
     return;
   }
   jobjectArray objKeyArray=(*env)->CallObjectMethod(env,objSet,midToArray);

   if(objKeyArray == NULL) {
      return;
   }
    jclass clsArrays=(*env)->FindClass(env,"java/util/Arrays");

    if(clsArrays==NULL){
      return;
    }
    jmethodID midSort=(*env)->GetStaticMethodID(env,clsArrays,"sort","([Ljava/lang/Object;)V");

    (*env)->CallStaticVoidMethod(env, clsArrays, midSort, objKeyArray);

    int size=(*env)->GetArrayLength(env,objKeyArray);
    LOGI("size is %d\n",size);
    int i;
    jmethodID midMapGetValue = (*env)->GetMethodID(env, clasParms, "get", "(Ljava/lang/Object;)Ljava/lang/Object;");

    jclass clsStr=(*env)->FindClass(env,"java/lang/String");
    jmethodID midGetBytes=(*env)->GetMethodID(env,clsStr,"getBytes","()[B");

   for(i=0;i<size;i++){
      LOGI("i is %d\n",i);
     //获取可以和值
     jstring objKeyI = (*env)->GetObjectArrayElement(env, objKeyArray, i);

    //get value by key
    jobject objValueI = (*env)->CallObjectMethod(env, params, midMapGetValue, objKeyI);
      LOGI("key is %s\n",(*env)->GetStringUTFChars(env,objKeyI,NULL));

       if(objValueI!=NULL){
         jbyteArray jbyteArrayKey=(*env)->CallObjectMethod(env,objKeyI,midGetBytes);
         jbyte *jbyteKey=(*env)->GetByteArrayElements(env,jbyteArrayKey,NULL);
         int keyLen = (*env)->GetArrayLength(env, jbyteArrayKey);
      }
    }
}





/**
 * 获取签名
 */

 void getPublicKey(JNIEnv *env){
    jclass jcs=(*env)->FindClass(env,"mama/ndklib/JC");
    if(NULL==jcs){
        return;
    }
    jmethodID minGetPubilcKey=(*env)->GetStaticMethodID(env,jcs,"getPublicKey","()[B");
    if(NULL==minGetPubilcKey){
        return;
    }
    jbyteArray publicKey = (*env)->CallStaticObjectMethod(env, jcs, minGetPubilcKey);
    if(publicKey == NULL) {
        return;
    }
    int len = (*env)->GetArrayLength(env, publicKey);
    LOGI(" publicKey len is %d \n",len);
}





  //获取渠道号
  string getchel(JNIEnv *env){
        jclass js=(*env)->FindClass(env,"mama/ndklib/JC");
        if(NULL==js){
          LOGI("not find%s\n","js 引用");
          return NULL;
       }
        jmethodID minGetContext=(*env)->GetStaticMethodID(env,js,"getAppContext","()Landroid/content/Context;");
        if(NULL==minGetContext){
          LOGI("not find%s\n","minGetContext 引用");
          return NULL;
        }

        jobject context=(*env)->CallStaticObjectMethod(env,js,minGetContext);
        if(NULL==context){
          LOGI("not find%s\n","context 引用");
          return NULL;
        }

       jclass clsCheannle=(*env)->FindClass(env,"com/umeng/analytics/AnalyticsConfig");
       jmethodID minGetChannel=(*env)->GetStaticMethodID(env,clsCheannle,"getChannel","(Landroid/content/Context;)Ljava/lang/String;");
       jstring jChannel=(*env)->CallStaticObjectMethod(env, clsCheannle, minGetChannel, context);
       if(jChannel == NULL) {
          return NULL;
       }
      const char *cChannel=  (*env)->GetStringUTFChars(env, jChannel, NULL);
      if(NULL==cChannel){
          return NULL;
      }
      LOGI(" cChannel %s\n",cChannel);
       char *channel = malloc(strlen(cChannel) + 1);
      strcpy(channel, cChannel);
      (*env)->ReleaseStringUTFChars(env, jChannel, cChannel);
      return channel;
  }

   //检查是不是debug
  jboolean checkEnvironment(JNIEnv *env) {
    //1获取对象
    jclass js=(*env)->FindClass(env,"mama/ndklib/JC");
    if(NULL==js){
        LOGI("not find%s\n","找不到jclass 引用");
        return 0;
    }
    //获取对象的ID
    jmethodID midIsDebugable=(*env)->GetStaticMethodID(env,js, "isDebugable","()Z");
    if(NULL==midIsDebugable){
        LOGI("not find%s\n","找不到jmethodID 引用");
        return 0;
    }
    //拿到值
    jboolean isDebug=(*env)->CallStaticBooleanMethod(env, js, midIsDebugable);
    if(isDebug) {
        LOGI("isDebugable :%s\n","Debugable");
         char *channel= getchel(env);
        if(channel!=NULL){
            if(strcmp(channel, "debug") == 0) {
                LOGI("isDebugable :%s\n","ok");
                return 1;
            }

            //free memory
            free(channel);
        }

        getPublicKey(env);

    }else{
        LOGI("isDebugable :%s\n","NODebugable");
    }

    return  isDebug;
}

/**
 * 修改实例变量str的值
 */
    JNIEXPORT void JNICALL Java_mama_ndklib_JC_accessInstanceField(JNIEnv *env, jclass cls, jobject obj){
    jclass clazz;
    jfieldID fid;
    jstring j_str;
    jstring j_newStr;
    const char *c_str=NULL;

    //1获取类的class 引用
     clazz=(*env)->GetObjectClass(env,obj);

      if(clazz==NULL){
        LOGI("not find%s\n","找不到class 引用");
         return;
       }

      //2获取类的实例变量str的属性ID
       fid=(*env)->GetFieldID(env,clazz,"str","Ljava/lang/String;");
        if(fid==NULL){
          LOGI("not find%s\n","找不到class 引用");
          return;
        }
       // 3. 获取实例变量str的值
       j_str = (jstring)(*env)->GetObjectField(env,obj,fid);

       if(j_str==NULL){
        LOGI("not find%s\n","找不到j_str 引用");
        return;
       }

       //4unicode 编码的java字符串转成c风格的字符串
       c_str=(*env)->GetStringUTFChars(env,j_str,NULL);

        if(c_str==NULL){
        LOGI("not find%s\n","找不到c_str 引用");
        return;
        }
        LOGI("c_str :%s\n",c_str);

        (*env)->ReleaseStringUTFChars(env,j_str,c_str);

        //5修改实例变量str的值
        j_newStr=(*env)->NewStringUTF(env,"this is C string");
        if (j_newStr == NULL) {
          return;
        }

       (*env)->SetObjectField(env, obj, fid, j_newStr);
       checkEnvironment(env);
        // 6.删除局部引用
        (*env)->DeleteLocalRef(env, clazz);
        (*env)->DeleteLocalRef(env, j_str);
        (*env)->DeleteLocalRef(env, j_newStr);


}


/**
 * 修改静态变量的
 */
     JNIEXPORT void JNICALL Java_mama_ndklib_JC_accessStaticField(JNIEnv *env, jclass cls){
       /* jclass clazz;
        jfieldID fid;
        jint num;

        //1.获取ClassField类的Class引用
        clazz = (*env)->FindClass(env,"mama/ndklib/ClassField");
        if (clazz == NULL) {    // 错误处理
        return;
        }


        //2.获取ClassField类静态变量num的属性ID
        fid = (*env)->GetStaticFieldID(env, clazz, "num", "I");
        if (fid == NULL) {
        return;
        }

        // 3.获取静态变量num的值
        num = (*env)->GetStaticIntField(env,clazz,fid);
        // 4.修改静态变量num的值
        (*env)->SetStaticIntField(env, clazz, fid, 80);

        // 删除属部引用
        (*env)->DeleteLocalRef(env,clazz);*/
}

